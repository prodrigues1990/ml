from numpy import random, array, exp, dot

from perceptron import Perceptron

def sigmoid(x):
    return 1 / (1 + exp(-x))

def sigmoid_derivative(x):
    return x * (1 - x)

class MultiLayerPerceptron:
    def __init__(self, number_of_inputs):
        self.first_layer = Perceptron(number_of_inputs, number_of_inputs + 1)
        self.second_layer = Perceptron(number_of_inputs + 1, 1)

    def train(self, inputs, expected, iterations=10000):
        for iteration in range(iterations):
            first_layer_output = self.first_layer.classify(inputs)
            second_layer_output = self.second_layer.classify(first_layer_output)

            second_layer_error = expected - second_layer_output
            second_layer_delta = second_layer_error * sigmoid_derivative(second_layer_output)
            second_layer_adjustments = dot(first_layer_output.T, second_layer_delta)

            first_layer_error = dot(second_layer_delta, self.second_layer.weights.T)
            first_layer_delta = first_layer_error * sigmoid_derivative(first_layer_output)
            first_layer_adjustments = dot(inputs.T, first_layer_delta)

            self.first_layer.weights += first_layer_adjustments
            self.second_layer.weights += second_layer_adjustments

    def classify(self, inputs):
        first_layer_output = self.first_layer.classify(inputs)
        return self.second_layer.classify(first_layer_output)

if __name__ == '__main__':
    ## Linear Case

    # create a multi layer Perceptron with 3 inputs (and one output)
    random.seed(1)
    perceptron = MultiLayerPerceptron(number_of_inputs=3)

    # train the perceptron on a known dataset
    # initially internal weights are random, and during the train process they gradually tend
    # towards the desired output, as long as there is linear relation between the inputs and
    # outputs
    train_inputs = array([
        [0, 0, 1],
        [1, 1, 0],
        [1, 0, 1]])
    # 3rd column is irrelevant in this case, if either column 1 or 2 are 1 the output is 1
    train_outputs = array([[0, 1, 1]]).T
    perceptron.train(train_inputs, train_outputs, 1000)

    # we ask the perceptron to classify an unseen case
    new_case = array([0, 1, 1])
    case_solution = 0
    prediction = perceptron.classify(new_case)
    print(f'linear case\t prediction {prediction[0]:.03f}, expected {case_solution}')


    ## XOR Case (Non-Linear)

    # create multi layer Perceptron with 3 inputs (and one output)
    random.seed(1)
    perceptron = MultiLayerPerceptron(number_of_inputs=3)

    # train the perceptron on a known dataset
    # initially internal weights are random, and during the train process they gradually tend
    # towards the desired output, as long as there is linear relation between the inputs and
    # outputs
    train_inputs = array([
        [1, 1, 0],
        [1, 0, 1],
        [0, 1, 0],
        [0, 0, 1]])
    # 3rd column is irrelevant in this case, if either column 1 or 2 are 1 the output is 1
    train_outputs = array([[0, 1, 1, 0]]).T
    perceptron.train(train_inputs, train_outputs, 1000)

    # we ask the perceptron to classify an unseen case
    new_case = array([0, 1, 1])
    case_solution = 1
    prediction = perceptron.classify(new_case)
    print(f'xor case\t prediction {prediction[0]:.03f}, expected {case_solution}')
