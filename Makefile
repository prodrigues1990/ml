.PHONY: help prepare-dev test lint shell

VENV_NAME?=venv
VENV_ACTIVATE=. $(VENV_NAME)/bin/activate
PYTHON=${VENV_NAME}/bin/python3.7

.DEFAULT: help
help:
	@echo "make venv"
	@echo "       setup virtual environment (rebuilds if requirements.txt changes)"
	@echo "make test"
	@echo "       run tests"
	@echo "make lint"
	@echo "       run linter"
	@echo "make shell"
	@echo "       run a python shell"

venv: $(VENV_NAME)/bin/activate
$(VENV_NAME)/bin/activate: requirements.txt
	test -d $(VENV_NAME) || virtualenv -p python3.7 $(VENV_NAME)
	${PYTHON} -m pip install -U pip
	${PYTHON} -m pip install -r requirements.txt
	touch $@

prepare-dev:
	sudo apt-get -y install python3.7 python3-pip
	python3 -m pip install virtualenv
	make venv

install: venv
	pip install --editable .

test: venv
	${PYTHON} -m pytest

lint: venv
	${PYTHON} -m pylint glreports

shell: venv
	${PYTHON} -ic 'import glreports'
