from numpy import random, array, exp, dot

def sigmoid(x):
    return 1 / (1 + exp(-x))

def sigmoid_derivative(x):
    return x * (1 - x)

class Perceptron:
    def __init__(self, number_of_inputs, number_of_neurons=1):
        self.weights = 2 * random.random((number_of_inputs, number_of_neurons)) - 1

    def train(self, inputs, expected, iterations=10000):
        for iteration in range(iterations):
            _, delta = self.classification_delta(inputs, expected)
            self.adjust(inputs, delta)

    def classify(self, inputs):
        inputs = inputs.astype(float)
        output = sigmoid(dot(inputs, self.weights))
        return output

    def classification_delta(self, inputs, expected):
        output = self.classify(inputs)
        delta = (expected - output) * sigmoid_derivative(output)
        return output, delta

    def adjust(self, inputs, delta):
        self.weights += dot(inputs.T, delta)

if __name__ == '__main__':
    ## Linear Case

    # create single Perceptron with 3 inputs (and one output)
    random.seed(1)
    perceptron = Perceptron(number_of_inputs=3)

    # train the perceptron on a known dataset
    # initially internal weights are random, and during the train process they gradually tend
    # towards the desired output, as long as there is linear relation between the inputs and
    # outputs
    train_inputs = array([
        [0, 0, 1],
        [1, 1, 0],
        [1, 0, 1]])
    # 3rd column is irrelevant in this case, if either column 1 or 2 are 1 the output is 1
    train_outputs = array([[0, 1, 1]]).T
    perceptron.train(train_inputs, train_outputs, 1000)

    # we ask the perceptron to classify an unseen case
    new_case = array([0, 1, 1])
    case_solution = 0
    prediction = perceptron.classify(new_case)
    print(f'linear case\t prediction {prediction[0]:.03f}, expected {case_solution}')


    ## XOR Case (Non-Linear)

    # create single Perceptron with 3 inputs (and one output)
    random.seed(1)
    perceptron = Perceptron(number_of_inputs=3)

    # train the perceptron on a known dataset
    # initially internal weights are random, and during the train process they gradually tend
    # towards the desired output, as long as there is linear relation between the inputs and
    # outputs
    train_inputs = array([
        [1, 1, 0],
        [1, 0, 1],
        [0, 1, 0],
        [0, 0, 1]])
    # 3rd column is irrelevant in this case, if either column 1 or 2 are 1 the output is 1
    train_outputs = array([[0, 1, 1, 0]]).T
    perceptron.train(train_inputs, train_outputs, 1000)

    # we ask the perceptron to classify an unseen case
    new_case = array([0, 1, 1])
    case_solution = 1
    prediction = perceptron.classify(new_case)
    print(f'xor case\t prediction {prediction[0]:.03f}, expected {case_solution} < does not look good')
